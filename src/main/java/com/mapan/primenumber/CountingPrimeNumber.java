/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mapan.primenumber;

/**
 *
 * @author edyprayitno
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountingPrimeNumber {
    public static void main(String args[]) throws IOException {
        
        
        System.out.print("Put your number : ");
        BufferedReader reader =  
                   new BufferedReader(new InputStreamReader(System.in)); 
        
        String value = reader.readLine();
        int number = 0;
        if (!value.equals(null))
            number = Integer.parseInt(value); 
        int count = 0;
        int i = 2;
    
        while(true) {
            boolean isPrime = true;
            
            for (int j = 2; j < i; j++) {
                if(i%j==0){
                    isPrime = false;
                    break;
                }
            }
    
            if(isPrime==true)
                count += 1;
                
             
            if(count==number)
            {
                System.out.println(number + " prime number : " + i);
                break;
            }
                
            ++i;
        }
    }
}

